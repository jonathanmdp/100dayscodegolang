package main

import (
	"context"
	"net"

	"github.com/sirupsen/logrus"
)

const maxBufferSize = 1024

func errorHandler(ctx context.Context, doneChan chan error) {
	select {
	case <-ctx.Done():
		logrus.Error("Context was unexpected closed", ctx.Err())
	case err := <-doneChan:
		logrus.Error("aplicattion channel was finished by error:", err)
	}
}

func serverUDP(ctx context.Context, address string) {
	packets, err := net.ListenPacket("udp", address)
	if err != nil {
		logrus.Errorf("Failed to get %s packets - error: %s", address, err)
	}
	defer packets.Close()
	logrus.Infof("Packets received from %s", address)
	doneChan := make(chan error, 1)
	buffer := make([]byte, maxBufferSize)

	go func() {
		for {
			nRead, addr, err := packets.ReadFrom(buffer)
			if err != nil {
				doneChan <- err
				return
			}
			logrus.Infof("packet-received: message=%s from=%s", string(buffer[:nRead]), addr.String())
			nRead, err = packets.WriteTo(buffer[:nRead], addr)
			if err != nil {
				doneChan <- err
				return
			}
			logrus.Infof("packet-written: bytes=%d to=%s", nRead, addr.String())
		}
	}()
	errorHandler(ctx, doneChan)
}

func main() {
	ctx := context.Background()
	serverUDP(ctx, "127.0.0.1:1234")
}
