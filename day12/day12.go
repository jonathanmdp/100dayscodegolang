package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"regexp"
	"strconv"
)

func GetAccumulatedMessagesAmount(exporterLog string) (int, error) {
	findLineInExporter, err := regexp.Compile("rice{durable=\"true\",policy=\"unknow\",queue=\"bean\",vhost=\"/\"} ([0-9]+)")
	handleError(err)
	findMessagesAmount, err := regexp.Compile("[0-9]+$")
	handleError(err)
	lineFound := findLineInExporter.FindString(exporterLog)
	return strconv.Atoi(findMessagesAmount.FindString(lineFound))
}

func handleError(err error) {
	if err != nil {
		logrus.Error("message error: ", err)
	}
}

func main() {
	/*
		Here our target is a value 61 in this line
		rice{durable="true",policy="unknow",queue="bean",vhost="/"} 61
	*/
	amountMessages, err := GetAccumulatedMessagesAmount("rice{durable=\"true\",policy=\"unknow\",queue=\"bean-error\",vhost=\"/\"} 0\nrice{durable=\"true\",policy=\"unknow\",queue=\"meat\",vhost=\"/\"} 0\nrice{durable=\"true\",policy=\"unknow\",queue=\"barbecue\",vhost=\"/\"} 0\nrice{durable=\"true\",policy=\"unknow\",queue=\"bean\",vhost=\"/\"} 61\n# HELP rice_ram Number of messages from messages_ready which are resident in ram.\n# TYPE rice_ram gauge\nrice_ram{durable=\"false\",policy=\"unknow\",queue=\"rice.bean\",vhost=\"/\"} 0\nrice_ram{durable=\"false\",policy=\"unknow\",queue=\"bean.rice\",vhost=\"/\"} 0")
	handleError(err)
	fmt.Println(amountMessages)
}
