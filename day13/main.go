package main

import (
	"flag"
	"fmt"
	"net"
	"time"
)

func scanPort(protocol, hostname string, port int) bool {
	address := fmt.Sprintf("%s:%d", hostname, port)
	conn, err := net.DialTimeout(protocol, address, 1*time.Minute)
	if err != nil {
		return false
	}
	defer conn.Close()
	return true
}

func main() {
	var hostname string
	var port int
	flag.IntVar(&port, "port", 0, "--hostname=<PORT_INT>")
	flag.StringVar(&hostname, "hostname", "", "--hostname=<HOSTNAME_STRING>")
	flag.Parse()
	fmt.Println("Port Scanning")
	open := scanPort("tcp", hostname, port)
	fmt.Printf("is Port %d of %s open? \n %t\n", hostname, port, open)
}
