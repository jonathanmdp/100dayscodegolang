package main

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/yuin/gluamapper"
	lua "github.com/yuin/gopher-lua"
)

type person struct {
	Name string
	Age  int
}

func main() {
	//Get Tables
	var person person
	L := lua.NewState()
	err := L.DoFile("script.lua")
	if err != nil {
		logrus.Fatal("Fail to get lua file: ", err)
	}
	err = gluamapper.Map(L.GetGlobal("person").(*lua.LTable), &person)
	if err != nil {
		logrus.Fatal("Fail to map person atributes: ", err)
	}
	fmt.Printf("Table/Struct data: %s %d \n", person.Name, person.Age)

	//Get functions
	co, _ := L.NewThread()
	function := L.GetGlobal("CallHello").(*lua.LFunction)
	_, err, values := L.Resume(co, function)
	fmt.Println("Lua function value: ", values[0])
}
