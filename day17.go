package main

import "fmt"

func parseArrayToString(array [3]string, max int) string {
	var phrase string
	for i := 0; i <= max; i++ {
		phrase = fmt.Sprint(phrase + array[i])
	}
	return phrase
}

func bruteForce(password string) {
	arr := [3]string{}
	for actual := 0; actual <= 3; actual++ {
		for i := 65; i <= 67; i++ {
			arr[actual] = fmt.Sprintf("%c", i)
			phrase := parseArrayToString(arr, actual)
			if password == phrase {
				fmt.Println("Password is ", phrase)
				return
			}
			fmt.Println(phrase)
		}
	}
}

func main() {
	bruteForce("A")
}
