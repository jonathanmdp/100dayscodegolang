package main

import (
	"fmt"
	"log"
	"time"
)

func runningtime(s string) (string, time.Time) {
	log.Println("Start:	", s)
	return s, time.Now()
}

func track(s string, startTime time.Time) {
	endTime := time.Now()
	log.Println("End:	", s, "took", endTime.Sub(startTime))
}

func bruteForceStatic(password string) {
	defer track(runningtime("execute"))
	combinations := 0
	doneChan := make(chan string)
	for i := 33; i <= 126; i++ {
		go func(i int) {
			for j := 33; j <= 126; j++ {
				go func(i, j int) {
					for k := 33; k <= 126; k++ {
						go func(i, j, k int) {
							for l := 33; l <= 126; l++ {
								combinations++
								phrase := fmt.Sprintf("%c%c%c%c", i, j, k, l)
								if password == phrase {
									doneChan <- fmt.Sprintln("Password is ", phrase, "combinations:", combinations)
									return
								}
							}
						}(i, j, k)
					}
				}(i, j)
			}
		}(i)
	}
	fmt.Println(<-doneChan)
}

func main() {
	bruteForceStatic("~~~~")
}
