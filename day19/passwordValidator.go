package day19

import (
	"fmt"
	"regexp"
)

func isPasswordValid(password string) (bool, error) {
	onlyNumbersRGX, err := regexp.Compile("^[0-9]+$")
	if err != nil {
		return false, fmt.Errorf("failed on compile regex expression")
	}
	if !onlyNumbersRGX.Match([]byte(password)) || len(password) > 6 {
		return false, fmt.Errorf("password is invalid")
	}
	return true, nil
}
