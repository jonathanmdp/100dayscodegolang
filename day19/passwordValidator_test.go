package day19

import (
	"github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
	"testing"
)

var _ = ginkgo.Describe("Sad Way", func() {
	ginkgo.It("Use a invalid password that uses more than 6 digits and receive false and a message error", func() {
		password := "1234567"
		validatorResult, err := isPasswordValid(password)
		gomega.Expect(err).Should(gomega.HaveOccurred())
		gomega.Expect(validatorResult).Should(gomega.BeFalse())
	})
	ginkgo.It("Use a invalid password that uses letters in password", func() {
		password := "1234ab"
		validatorResult, err := isPasswordValid(password)
		gomega.Expect(err).Should(gomega.HaveOccurred())
		gomega.Expect(validatorResult).Should(gomega.BeFalse())
	})
})

var _ = ginkgo.Describe("Happy way", func() {
	ginkgo.It("Use a valid password and receive true", func() {
		password := "123456"
		validatorResult, err := isPasswordValid(password)
		gomega.Expect(err).ShouldNot(gomega.HaveOccurred())
		gomega.Expect(validatorResult).Should(gomega.BeTrue())
	})
})

func TestPasswordValidator(t *testing.T) {
	gomega.RegisterTestingT(t)
	gomega.RegisterFailHandler(ginkgo.Fail)
	ginkgo.RunSpecs(t, "Sad Way")
	ginkgo.RunSpecs(t, "Happy Way")
}
