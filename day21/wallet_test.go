package day21

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"testing"
)

var _ = Describe("Trying explain TDD", func() {
	It("Using valid values to deposit money", func() {
		wallet := New(0)
		err := wallet.Deposit(10)
		Expect(err).ShouldNot(HaveOccurred())
		Expect(wallet.Balance()).Should(Equal(10))
	})
	It("Using invalid values to trying deposit money and should receive a error message", func() {
		wallet := New(0)
		err := wallet.Deposit(-1)
		Expect(err).Should(HaveOccurred())
		Expect(err.Error()).Should(Equal("Please use more money oO"))
	})
})

func TestWallet(t *testing.T) {
	RegisterTestingT(t)
	RegisterFailHandler(Fail)
	RunSpecs(t, "Trying explain TDD")
}
