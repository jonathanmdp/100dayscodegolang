package main

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
)

type Example struct {
	number int `json:"number"`
	nested *struct {
		arrayToUnderstand []string `json:"arrayToUnderstand"`
	} `json:"nested"`
}

func main() {
	var successExample *Example
	err := json.Unmarshal([]byte(fmt.Sprintf("%v", "{\"number\":\"1\",\"nested\":{\"arrayToUnderstand\":[\"valueHere\"]}}")), &successExample)
	if err != nil {
		logrus.Fatal("Unmarshal failed:", err)
	}
	testFunction(*successExample)
	testFunction2(successExample)

	var problematicExample1 *Example
	err = json.Unmarshal([]byte(fmt.Sprintf("%v", "{\"number\":\"1\",\"unexpected\":{\"arrayToUnderstand\":[\"valueHere\"]}}")), &problematicExample1)
	if err != nil {
		logrus.Fatal("Unmarshal failed:", err)
	}
	testFunction(*problematicExample1)
	testFunction2(problematicExample1)

	var problematicExample2 *Example
	err = json.Unmarshal([]byte(fmt.Sprintf("%v", "{\"number\":\"1\",\"nested\":{\"unexpected\":[\"valueHere\"]}}")), &problematicExample2)
	if err != nil {
		logrus.Fatal("Unmarshal failed:", err)
	}
	testFunction(*problematicExample2)
	testFunction2(problematicExample2)
}

func testFunction(example Example) {
	fmt.Println(example.nested == nil || example.nested.arrayToUnderstand == nil)
}

func testFunction2(example *Example) {
	fmt.Println(example.nested == nil || example.nested.arrayToUnderstand == nil)
}
