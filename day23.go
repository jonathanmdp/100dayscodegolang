package main

import "fmt"

type Person struct {
	name string
	age  int
}

func (p Person) whoAmI() string {
	return fmt.Sprint("i am ", p.name, p.age)
}

type trickPerson struct {
	Person
}

func main() {
	dad := trickPerson{}
	dad.name = "joao"
	dad.age = 44
	fmt.Println(dad.whoAmI())
}
