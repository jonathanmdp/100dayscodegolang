package main

import (
	"bytes"
	"fmt"
	. "github.com/onsi/gomega"
	"testing"
)

func TestGreet(t *testing.T) {
	RegisterTestingT(t)
	name := "Chris"
	buffer := bytes.Buffer{}
	Greet(&buffer, name)
	result := buffer.String()
	Expect(result).Should(Equal(fmt.Sprintf("Hello, %s", name)))
}
