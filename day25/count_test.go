package day25

import (
	"bytes"
	"fmt"
	"testing"

	. "github.com/onsi/gomega"
)

func TestCount(t *testing.T) {
	RegisterTestingT(t)
	buffer := &bytes.Buffer{}
	sleeperSpy := &SleeperSpy{}
	count(buffer, sleeperSpy)
	result := buffer.String()
	expected := fmt.Sprintf("3\n2\n1\nVai!")
	expectedSecondsPassed := 4
	Expect(result).Should(Equal(expected))
	Expect(sleeperSpy.Calls).Should(Equal(expectedSecondsPassed))
}
