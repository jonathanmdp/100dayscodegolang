package main

import (
	"testing"

	. "github.com/onsi/gomega"
)

func mockVerifierWebsite(url string) bool {
	if url == "waat://furhurtewe.geds" {
		return false
	}
	return true
}

func TestWebSiteVerifier(t *testing.T) {
	RegisterTestingT(t)
	websites := []string{
		"http://google.com",
		"http://blog.gypsydave5.com",
		"waat://furhurterwe.geds",
	}
	result := verifyWebsite(mockVerifierWebsite, websites)
	expected := map[string]bool{
		"http://google.com":          true,
		"http://blog.gypsydave5.com": true,
		"waat://furhurterwe.geds":    false,
	}
	Expect(result).Should(Equal(expected))
}
