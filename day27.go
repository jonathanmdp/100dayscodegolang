package main

import (
	"fmt"
	"time"
)

type sleeper interface {
	sleep()
}

type injectSleeper struct {
	Calls int
}

func (s *injectSleeper) sleep() {
	time.Sleep(1 * time.Second)
}

func simpleChannel(message string) {
	messages := make(chan string)

	go func(internalMessage string) {
		messages <- internalMessage
	}(message)
	fmt.Println(<-messages)
}

func bufferingChannel(bufferMessage [2]string) {
	messages := make(chan string, 2)
	messages <- bufferMessage[0]
	messages <- bufferMessage[1]

	fmt.Println(<-messages)
	fmt.Println(<-messages)
}

func mockSystemBehavior(sleeperBehavior *injectSleeper) {
	fmt.Println("Working")
	sleeperBehavior.sleep()
	fmt.Println("done")
}

func worker(done chan bool, internalSleeper *injectSleeper) {
	mockSystemBehavior(internalSleeper)
	done <- true
}

func syncronizeChannel(injectionSleeper *injectSleeper) {
	done := make(chan bool, 1)
	go worker(done, injectionSleeper)
	<-done
}

func ping(pings chan<- string, message string) {
	pings <- message
}

func pong(pings <-chan string, pongs chan<- string) {
	message := <-pings
	pongs <- message

}

func channelDirection() {
	pings := make(chan string, 1)
	pongs := make(chan string, 1)
	ping(pings, "Passed message")
	pong(pings, pongs)
	fmt.Println(<-pongs)
}

func main() {
	simpleChannel("simple channel ping")
	bufferingChannel([2]string{"buffering message", "second buffering message"})
	syncronizeChannel(&injectSleeper{})
	channelDirection()
}
