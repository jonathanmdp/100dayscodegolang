package main

import "fmt"

//General structure
type family interface {
	data() string
}

type person struct {
	name string
	age  int
}

//dad Structure
type dad struct {
	person
	occupation string
}

func NewDad(dadData dad) *dad {
	return &dad{dadData.person, dadData.occupation}
}

func (d dad) data() string {
	return fmt.Sprintf("Nome: %s, Idade: %d", d.name, d.age)
}

//son Structure
type son struct {
	person
	email string
}

func NewSon(sonData son) *son {
	return &son{sonData.person, sonData.email}
}

func (s son) data() string {
	return fmt.Sprintf("Nome: %s, Idade: %d, Email: %s", s.name, s.age, s.email)
}

func main() {
	dad := NewDad(dad{
		person{
			name: "joão",
			age:  50,
		},
		"doctor",
	})
	fmt.Println(dad.data())

	son := NewSon(son{
		person{
			name: "joãozinho",
			age:  50,
		},
		"joãozinho",
	})
	fmt.Println(son.data())

}
