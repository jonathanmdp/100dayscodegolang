package main

import (
	"context"
	"fmt"
)

func main() {
	defer fmt.Println("world")
	fmt.Println("hello")
	channel := make(chan string)
	ctx := context.Background()
	defer ctx.Done()
	ctx = addValue(ctx)
	go readValue(ctx, channel)
	fmt.Println(<-channel)
}

func addValue(ctx context.Context) context.Context {
	return context.WithValue(ctx, "key", "value")
}

func readValue(ctx context.Context, channel chan<- string) {
	value := ctx.Value("key")
	channel <- fmt.Sprint(value)
}
