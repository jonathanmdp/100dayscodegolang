module gitlab.com/figur8/100dayscodegolang

go 1.13

require (
	github.com/fatih/color v1.10.0
	github.com/hajimehoshi/go-mp3 v0.3.1
	github.com/hajimehoshi/oto v0.7.0
	github.com/hegedustibor/htgo-tts v0.0.0-20201020054059-2b0b3a7cc05a
	github.com/mitchellh/mapstructure v1.4.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/onsi/ginkgo v1.12.1
	github.com/onsi/gomega v1.10.3
	github.com/sirupsen/logrus v1.7.0
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/yuin/gluamapper v0.0.0-20150323120927-d836955830e7
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da
	golang.org/x/sys v0.0.0-20201126233918-771906719818 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.24.0 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
)
